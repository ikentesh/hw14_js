"use strict";

const button = document.querySelector(".button");
const body = document.body;
const headerLogo = document.querySelector(".logo__color-text");
const footerList = document.querySelectorAll(".footer-list__link");
const arrayFooterList = [...footerList];
const footerText = document.querySelector(".footer__text");
const mainText = document.querySelectorAll(".main__text");
const arrayMainText = [...mainText];
body.style.backgroundColor = getColor();

if (!localStorage.getItem("background")) {
  localStorage.setItem("background", "white");
}

function getColor() {
  return localStorage.getItem("background");
}

button.addEventListener("click", () => {
  if (localStorage.getItem("background") === "white") {
    localStorage.setItem("background", "black");
    headerLogo.style.color = "white";
    arrayFooterList.forEach((element) => {
      element.style.color = "white";
    });
    arrayMainText.forEach((element) => {
      element.style.color = "white";
    });
    footerText.style.color = "white";
  } else if (localStorage.getItem("background") === "black") {
    localStorage.setItem("background", "white");
    headerLogo.style.color = "black";
    arrayFooterList.forEach((element) => {
      element.style.color = "black";
    });
    arrayMainText.forEach((element) => {
      element.style.color = "black";
    });
    footerText.style.color = "black";
  }
  body.style.backgroundColor = getColor();
});

if (body.style.backgroundColor === "black") {
  headerLogo.style.color = "white";
  arrayFooterList.forEach((item) => {
    item.style.color = "white";
  });
  arrayMainText.forEach((element) => {
    element.style.color = "white";
  });
  footerText.style.color = "white";
}

if (body.style.backgroundColor === "white") {
  headerLogo.style.color = "black";
  arrayFooterList.forEach((item) => {
    item.style.color = "black";
  });
  arrayMainText.forEach((element) => {
    element.style.color = "black";
  });
  footerText.style.color = "black";
}
